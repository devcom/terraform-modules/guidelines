# One Awesome Module

## Purpose

This module provides awesome sauce.

## Description

Using a combination of VT and AWS awesomeness awesome sauce is generated.
See the architectural diagram below:

Make sure you include an [architectural diagram](https://aws.amazon.com/architecture/icons/)

## Usage Instructions

Copy and paste into your Terraform configuration, insert or update the
variables, and run `terraform init`:

```
module my_module {
  source = "git@code.vt.edu:devcom/terraform-modules/some-module.git?ref=v1.0"
  service_name = "my-service"
  environment = "production"
  responsible_party = "joe"
  vcs = "/path/to/some/git/repo"
}
```

## Preconditions and Assumptions

None

## Inputs

| Name | Description | Type | Default | Required |
| ---- | ----------- | ---- | ------- | -------- |
| service_name | The high level, usually customer facing service using this module. | string | - | yes |
| responsible_party | The person (pid) who is primarily responsible for the configuration and maintenance of the service. | string | - | yes |
| vcs | A link to the repo in a version control system (usually Git) that manages this resource. | string | - | yes |
| environment | Usually of 'development’, ‘test’, or ‘production’ | string | production | no |
| responsible_party2 | Back up for responsible_party | string | none | no |
| data_risk | ‘low’, ‘medium’ or ‘high’ based on data-risk classifications defined [here](http://it.vt.edu/content/dam/it_vt_edu/policies/Virginia-Tech-Risk-Classifications.pdf) | string | low | no |
| compliance_risk | 'none', ‘ferpa’ or ‘pii’ based on the compliance level of data being handled by this module | string | none | no |
| documentation | A link to documentation and/or history file | string | none | no |

## Outputs
This module provides no outputs.

## Versions

| Version | Major changes |
| ------- | ------------- |
| 1     | Created module |
| 2     | Added more sauce |
